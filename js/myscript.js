//Datatable
$('.info-data-table').DataTable({
"bLengthChange": false,
});

//Collapse
$(".button-collapse").sideNav();

//Select
$('select').material_select();

//Tooltip
$('.tooltipped').tooltip({delay: 50});

//Smooth scroll
$(".smoothscroll").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = "";
      });
    } // End if
  });

 
  $('.modal').modal();